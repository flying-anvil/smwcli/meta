# SMWCli TUI

TODO: Add Text

I'm not sure about the top row yet `(1)`. What can be offered beside SMW Hacks? Own Lunuar Magic projects? Other SMWC categories? Maybe a configuration page.

- `(2)` Tabs to switch between different "modes"/layouts/views
  - `Table`
  - `List`
  - `(Play) Sessions` 

## SMW Hack Table

![SMW Hack Table](SMW%20Hack%20Table.svg)

 Lists all locally known Hacks in a table format with some data.
 
 The table is interactive and a row can be selected using the arrow keys (↑ ↓) (this way it should also work on Steam Deck and perhaps similar devices). _Interacting_ with a row by using enter (⏎) summons a "popup" to select an action (see `(4)` below for details).

- `(3)` The main view here is the Hack table. It lists some basic inforamtion at a glance.
- `(4)` _Interacting_ with a row shows an "Action List", which offers some actions:
  - `Play` simply starts the Hack with the configured emulator
    - The file might not be present anymore. Prompt to re-download+apply/delete entry/ignore error
  - `Show (Play) Sessions` switches to the view of the same name, but filtered to only show play sessions for the selected Hack
  - `Open SMW Central Page` opens the Hack page on SMWCentral with the default browser.
  - Additional actions might include
    - `Clear {Play Time, First Played, Last Played}` to forget simple time trackings
    - `Clear (Play) Sessions` forgets all play sessions for this Hack
    - `Update` might check if an updated version of the Hack is available and applies it
    - `Update Details` fetches the Hack details and updates them in the local database (They should **not** be fetched on each display, we don't want to cause unneccesary traffic on SMWCentral). Maybe also add a cooldown (some minutes, details don't update that frequently) to it to further prevent unwanted requests
    - `…`


## SMW Hack List

![SMW Hack List](SMW%20Hack%20List.svg)

Similar to the table view, but more condensed.

This is a split view with two panes next to each other.
The left one shows only a list of Hack names `(1)`, while the right pane shows details about the currently selected entry `(2)`. The details probably should also include local data such as playtime.

_Interacting_ should also invoke the "popup" described in `SMW Hack Table`.

## (Play) Sessions

![SMW Hack List](SMW%20Hack%20Play%20Sessions.svg)

This is basically a graph/bar chart that shows how much activity happened when and with which Hack. Similar to the "Activity Log" of the 3DS/Wii U.
Starting/Playing Hacks though SMWCli tracks both the playtime (by measuring the time between emulator start and end) and play sessions on a per-Hack level.
Play sessions and total playtime are stored differently, with the total playtime being an ever increasing number, whereas play sessions are date periods (start-date and end-date).  
(_Both parts are configurable/deactivatable_)

- `(1)` is the graph/bar chart
  - Each column of the terminal is 1 day.
  - Each Hack is colored differently
    - It's important that the color stays consistent
      - Maybe hash the name and infer some color data from it
  - Displays information from now (right) back n days (left)
  - Horizontal scrolling might be something to look into.
- `(2)` is the legend, showing which color means which Hack

### Which blocks should be used?

- `[1]` ` ▁▂▃▄▅▆▇█` for more fine graindd vertical stages
- `[2]` ` ▏ ▎ ▍ ▌ ▋ ▊ ▉ █` (specifically the 7/8 or 6/8 wide one `▉`) to better distinguish columns
- Depending on the terminal height and the resolution of the tracked data, full-height blocks might be good enough
- Depending on the width, there might be enough sapce to only use every 2nd column
- My windowed terminal is `150 × 41` (width × height), maximized is `190 × 50`
  - Assuming the smaller one, that would be wide enough for 5 months for `[1]` or 2.5 months for `[1]` with spaces
  - The larger one would be 6.4 / 3.2 months
  - Accounting for layout "overhead", an estimate would be 4 / 2 months and 6 / 3 months
  - Assuming the smaller one, that would be high enough for ~30 vertically stacked blocks (already accounting for layout "overhead")
  - The larger one would be ~40
  - If each block is an hour, 24 would be enough if they are rounded to whole hours
    - Spending a whole day playing SMW ROM Hacks seems unlikely
    - However, there might be someone who is afk for a whole day :/
  - 48 blocks would allow to round to half an hour
  - The value of "max height" could be dynamic to always fit, also allowing for finer grained rounding

(_TODO: Research some common default terminal sizes_)

Personally I think that using `[1]` is the way to go. The close-together coulumns might not be that bad, or the gapped ones can still display enough information.

(_note that depending on font style/settings, the following examples might look different in a web browser than in a terminal_)
```
       ▁▂     
  ▁▅▇▇████▆▂  
▄███████████▆▁
```

```
▊ ▊▊   ▊
▊▊▊▊ ▊▊▊
▊▊▊▊▊▊▊▊▊
```

```
              ▁ ▂          
    ▁ ▅ ▇ ▇ █ █ █ █ ▆ ▂    
▄ █ █ █ █ █ █ █ █ █ █ █ ▆ ▁
```
