CREATE TABLE `hack_tags` (
    `hack_id`  TEXT,
    `tag_name` TEXT,
    PRIMARY KEY(hack_id, tag_name)
);
