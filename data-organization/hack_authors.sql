CREATE TABLE `hack_authors` (
    `hack_id`   TEXT NOT NULL,
    `author_id` TEXT NOT NULL,
    PRIMARY KEY(hack_id, author_id)
);
