# SMWCli data organization

- Use a local sqlite file/database to store all the information.
- Migrating to a newer version
  - On startup of `smwcli`, automatically create missing tables
- SMWCli might be able to import a directory of Hacks by searching SMWC, but that seems to be a thing for the far future
- How to deal with a corrupt database?
  - Throw it away and re-discover/import the data?
