CREATE TABLE `play_sessions` (
    `hack_id`     TEXT,
    `date_start`  TEXT,
    `date_end`    TEXT,
    `duration`    INTEGER,
    PRIMARY KEY(hack_id)
);
