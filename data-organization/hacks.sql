-- Only stuff that's relevant/available locally
CREATE TABLE `hacks` (
    `internal_id`      TEXT    NOT NULL CHECK(length(internal_id) = 16) UNIQUE,
    `smwc_id`          INTEGER, -- NULL for local Hacks
    `display_name`     TEXT    NOT NULL,
    `date_entry_added` TEXT    NOT NULL,
    `local_rating`     REAL    NOT NULL,
    `description`      TEXT    NOT NULL,
    `first_played`     TEXT    NOT NULL,
    `last_played`      TEXT    NOT NULL,
    `playtime`         INTEGER NOT NULL,
    PRIMARY KEY(internal_id)
);
