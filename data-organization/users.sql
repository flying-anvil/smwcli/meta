CREATE TABLE `users` (
    `internal_id` TEXT    NOT NULL UNIQUE,
    `smwc_id`     INTEGER,
    `name`        TEXT    NOT NULL,
    `color_hex`   TEXT,
    PRIMARY KEY(internal_id)
);
