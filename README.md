# SMWCli

CLI tool for interacting with SMW ROM Hacks and SMW Central

This repository contains non-code stuff, such as documents, diagrams and other things used for planning.

---

This tool currently mutates into a fully-fledged game manager for SMW ROM Hacks. See the `README.md` in `TUI` for further infirmation.

## Features

| Feature               | Description                                               | State         |
|-----------------------|-----------------------------------------------------------|---------------|
| CLI Hack Manager      | See `TUI` for details                                     | Planninng     |
| - Local database      | Store information about locally available Hacks           | Planninng     |
| - TUI Layout          | Basic TUI layout using the `tui` crate                    | Planninng     |
| - Interactive Table   | Navigatable table that shows available Hacks              | Planninng     |
| - Interactive List    | Navigatable side-by-side list of Hack names and details   | Planninng     |
| - Interaction menu    | Menu to do different things with the selected Hack        | Planninng     |
| - Play Session Tracker| Track play sessions and display stats in a chart          | Planninng     |
|                       |                                                           |               |
| SMW Central Client    | Provide a client for fetching data from SMWC. Currently only for SMW Hacks | Migrating     |
|                       |                                                           |               |
| Thumbnails            | It should be possible to use the 1st screenshot as a thumbnail for the ROM file. There's a [freedesktop specification](https://specifications.freedesktop.org/thumbnail-spec/thumbnail-spec-latest.html) for that. | Wanted        |
|                       |                                                           |               |
